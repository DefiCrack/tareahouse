package modulo2;

import java.util.Scanner;

public class Ejer3Punto2 {

	public static void main(String[] args) { //Determinar si un n�mero ingresado es par o impar
		int val;
		
		System.out.println("Ingrese el n�mero deseado");
		
		try  (Scanner scan = new Scanner(System.in)){
			val = scan.nextInt();
			
		System.out.println("valor= " + val);	
			
			if (val % 2 == 0) {
				System.out.println("el n�mero es par");
			}else {
				System.out.println("el n�mero es impar");
			}
		}
	}
}
