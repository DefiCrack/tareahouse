package modulo2;

import java.util.Scanner;

public class Ejer3Punto5 {

	public static void main(String[] args) { //Dar la medalla correspondiente de acuerdo al puesto ingresado
		int puesto;
		
		try  (Scanner scan = new Scanner(System.in)){
			System.out.println("Ingrese su puesto (1,2,3... etc)");
			puesto = scan.nextInt();
			if (puesto == 1){
				System.out.println("Usted ha conseguido la medalla de oro");
			}else if(puesto == 2){
				System.out.println("Usted ha conseguido la medalla de plata");
			}else if(puesto == 3){
				System.out.println("Usted ha conseguido la medalla de bronce");
			}else if(puesto > 3) {
				System.out.println("Segu� participando");
			}
		}
	}
}