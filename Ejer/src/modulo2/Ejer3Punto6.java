package modulo2;

import java.util.Scanner;

public class Ejer3Punto6 {

	public static void main(String[] args) { //Ordenar por cursos dependiendo del n�mero ingresado
		
		int num;
		try  (Scanner scan = new Scanner(System.in)){
			System.out.println("Ingrese el valor");
			num = scan.nextInt();

			if(num == 0){
				System.out.println("JARD�N DE INFANTES");
			}else if(num >= 1 && num <= 6) {
				System.out.println("PRIMARIA");
			}else if(num >= 7 && num <= 12) {
				System.out.println("SECUNDARIA");
			}			
		}
	}
}
