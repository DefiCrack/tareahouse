package modulo2;

import java.util.Scanner;

public class Ejer3Punto1 {

	public static void main(String[] args) { //Calcular promedio de 3 notas y ver si el alumno est� aprobado
		int res;
													
		try  (Scanner scan = new Scanner(System.in)){ 
		
			System.out.println("Ingrese la nota #1");
			
				int nota1 = scan.nextInt();
				System.out.println("El valor 1 es " + nota1); //Ingresan las notas
			
			System.out.println("Ingrese la nota #2");
			
				int nota2 = scan.nextInt();
				System.out.println("El valor 2 es " + nota2);      
				
			System.out.println("Ingrese la nota #3");
				
				int nota3 = scan.nextInt();
				System.out.println("El valor 3 es " + nota3);	
				
			System.out.println("Las calificaciones igresadas son: ");
			System.out.println(+ nota1);
			System.out.println(+ nota2); 
			System.out.println(+ nota3);
			
			if (nota1 > 10){ //revisando si existe un valor inv�lido
				System.out.println("Se ha ingresado un valor inv�lido, por favor reinicie el programa e intente de nuevo ");
			}else if (nota2 > 10){
				System.out.println("Se ha ingresado un valor inv�lido, por favor reinicie el programa e intente de nuevo ");
			}else if (nota3 > 10){
				System.out.println("Se ha ingresado un valor inv�lido, por favor reinicie el programa e intente de nuevo ");
			}else {
						
			res = (nota1 + nota2 + nota3 )/3; //c�lculo del promedio
			
				if (res >= 7 && res <=10){
					System.out.println("El alumno est� aprobado con una calificaci�n final de "+ res); //resultado final del promedio
				}else{  
					System.out.println("El alumno est� desaprobado con una calificaci�n final de "+ res);
				}	
			}
		}
	}
}